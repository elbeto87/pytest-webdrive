from selenium import webdriver

browser = webdriver.Chrome()
browser.get('https://techstepacademy.com/trial-of-the-stones')

input_riddleOfStone = "input[id='r1Input']"
input_riddleOfSecrets = "input[id='r2Input']"
button_riddleOfStone = "button[id='r1Btn']"
button_riddleOfSecrets = "button[id='r2Butn']"
answer_riddleOfStone = "div[id='passwordBanner']"
answer_riddleOfSecrets = "div[id='successBanner1']"
expectedResult_riddleOfStone = "bamboo"
expectedResult_riddleOfSecrets = "Success!"
jessica_xpath_value = "//div/label[text()='Total Wealth ($):']/../span[.='Jessica']/../p"
bernard_xpath_value = "//div/label[text()='Total Wealth ($):']/../span[.='Bernard']/../p"
name_richest_merchant_css = "input[id='r3Input']"
name_richest_merchant_css_button = "button[id='r3Butn']"
expectedResult_theTwoMerchants = "Success!"
successBanner_theTwoMerchants_css = "div[id='successBanner2']"
checkAnswers_css = "button[id='checkButn']"
checkAnswers_text_css = "div[id='trialCompleteBanner']"
checkAnswers_expected = "Trial Complete"

riddleOfStone = browser.find_element_by_css_selector(input_riddleOfStone)
riddleOfStone.send_keys("rock")

btn_riddleOfStone = browser.find_element_by_css_selector(button_riddleOfStone)
btn_riddleOfStone.click()
result = browser.find_element_by_css_selector(answer_riddleOfStone)

assert expectedResult_riddleOfStone == result.text
print("Riddle of Stone test has been passed")

browser.find_element_by_css_selector(input_riddleOfSecrets).send_keys(result.text)
browser.find_element_by_css_selector(button_riddleOfSecrets).click()
result = browser.find_element_by_css_selector(answer_riddleOfSecrets)

assert result.text == expectedResult_riddleOfSecrets
print("Riddle of Secrets test has been passed")

jessica_wealth = browser.find_element_by_xpath(jessica_xpath_value)
bernard_wealth = browser.find_element_by_xpath(bernard_xpath_value)

if int(jessica_wealth.text) > int(bernard_wealth.text):
    browser.find_element_by_css_selector(name_richest_merchant_css).send_keys('Jessica')
else:
    browser.find_element_by_css_selector(name_richest_merchant_css).send_keys('Bernard')

browser.find_element_by_css_selector(name_richest_merchant_css_button).click()

assert expectedResult_theTwoMerchants ==  browser.find_element_by_css_selector(successBanner_theTwoMerchants_css).text
print("The Two Merchants test has been passed")

browser.find_element_by_css_selector(checkAnswers_css).click()

assert checkAnswers_expected == browser.find_element_by_css_selector(checkAnswers_text_css).text
print("The trial has been completed")