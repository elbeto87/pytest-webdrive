from selenium import webdriver

browser = webdriver.Chrome()

browser.get('https://techstepacademy.com/training-ground')

input2_css_locator = "input[id='ipt2']"
bt4_xpath_locator = "//button[@id='b4']"
price2_xpath_locator = "$x(\"//b[text()='Product 1']/../../p\")"

#assign elements
input2_elem = browser.find_element_by_css_selector(input2_css_locator)
but4_elem = browser.find_element_by_xpath(bt4_xpath_locator)

#manipulate elements
input2_elem.send_keys("Test text")
but4_elem.click()
browser.quit()
